import express from "express";
const app = express();
/*
	In diesem Script verwenden wir Middleware auf verschiedene Arten:
	- inline
	- benannte Funktion
	- am App Objekt für alle routes

	Achtung, MW wird in der Reihenfolge der Zuweisung ausgeführt.
*/
const showDate = (req, res, next) => {
	const date = new Date()
	// Middleware kann den Request/die Response manipulieren
	// wir schreiben das Datum in den Request und können
	// es in den folgenden MW auslesen
	req.currentDate = date
	next()
}

const logger = (req, res, next) => {
	console.log("Logging into Database")
	next()
}

const dummy = (req, res, next) => {
	console.log("Dummy middleware")
	next()
}

// Eine Middleware, die nun von allen routes verwendet wird.
app.use(dummy)
app.use(logger)

app.get(
	"/",
	/*
		Unsere erste middleware als anonyme Funktion in unserer
		route. next() ruft die nächste middleware in der route auf.
		Jede Middleware erhält das Reqest Objekt, das Response
		Objekt und die nächste Middleware (default next) Funktion.
		Eine Middleware steigt entweder aus oder ruft bei Erfolg
		die nächste MW in der Kette auf.
	*/
	(req, res, next) => {
		console.log("First middleware")
		next();
	},
	// benannte Funktion übergeben
	showDate,
	// Diese Middleware ermittelt eine Fehler und unterbricht die Kette
	// (req, res, next) => {
	// 	console.log("Oh no, an error occured");
	// 	res.sendStatus(404)
	// 	// next();
	// },
	(req, res) => {
		console.log("Homepage")
		const date = req.currentDate
		res.send(`Hompage ${date}`);
	}
);

app.listen(3000, () => console.log("Listening to http://localhost:3000"));
